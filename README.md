
## Assign fixed /dev/* links to Arduinos

sudo cp udev/rules.d/55-roverrobotics.rules /etc/udev/rules.d/.
sudo udevadm control --reload-rules && sudo udevadm trigger
