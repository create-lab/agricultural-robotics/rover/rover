#! /usr/bin/env python3

import math

import rospy

import std_msgs.msg
import sensor_msgs.msg
import nav_msgs.msg

from create_common_rospy.simple_subscriber import SimpleSubscriber
from tf.transformations import euler_from_quaternion

class Arm2GravityBeta:
    def __init__(self) -> None:

        self.imu_subscriber = SimpleSubscriber("/zed2i/zed_node/imu/data", sensor_msgs.msg.Imu)
        self.leadout_subscriber = SimpleSubscriber("/rover/leadout_encoder/raw", std_msgs.msg.Float32)
    
        self.odometry_subscriber = SimpleSubscriber("/zed2i/zed_node/odom", nav_msgs.msg.Odometry)

        self.arm2gravity_beta_rad_publisher = rospy.Publisher("/rover/arm2gravity_beta/encoder/rad", std_msgs.msg.Float32, queue_size=10)
        self.arm2gravity_beta_deg_publisher = rospy.Publisher("/rover/arm2gravity_beta/encoder/deg", std_msgs.msg.Float32, queue_size=10)
        
        self.arm2gravity_cam_beta_rad_publisher = rospy.Publisher("/rover/arm2gravity_beta/camera/rad", std_msgs.msg.Float32, queue_size=10)
        self.arm2gravity_cam_beta_deg_publisher = rospy.Publisher("/rover/arm2gravity_beta/camera/deg", std_msgs.msg.Float32, queue_size=10)
        
        self.leadout_publisher = rospy.Publisher("/rover/leadout_encoder/deg", std_msgs.msg.Float32, queue_size=10)
        self.pitch_publisher = rospy.Publisher("/rover/pitch/deg", std_msgs.msg.Float32, queue_size=10)
        self.roll_publisher = rospy.Publisher("/rover/roll/deg", std_msgs.msg.Float32, queue_size=10)

        timer_period = rospy.Duration(0.04)
        self.timer = rospy.Timer(timer_period, self.timer_callback)

    def timer_callback(self, event: rospy.timer.TimerEvent) -> None:

        explicit_quat = [self.imu_subscriber.message.orientation.x, self.imu_subscriber.message.orientation.y, self.imu_subscriber.message.orientation.z, self.imu_subscriber.message.orientation.w]
        (roll, pitch, yaw) = euler_from_quaternion(explicit_quat)

        arm2gravity = self.leadout_subscriber.message.data + yaw
        if arm2gravity > math.pi:
            arm2gravity -= 2*math.pi

        self.arm2gravity_beta_rad_publisher.publish(std_msgs.msg.Float32(data=arm2gravity))
        self.arm2gravity_beta_deg_publisher.publish(std_msgs.msg.Float32(data=arm2gravity*180.0/math.pi))

        position = self.odometry_subscriber.message.pose.pose.position

        anchor_offset_x = 1.70
        # anchor_offset_y = 0.4
        anchor_offset_y = 0.0
        arm2gravity_camera = math.atan2(anchor_offset_y - position.y, anchor_offset_x- position.x)

        self.arm2gravity_cam_beta_rad_publisher.publish(std_msgs.msg.Float32(data=arm2gravity_camera))
        self.arm2gravity_cam_beta_deg_publisher.publish(std_msgs.msg.Float32(data=arm2gravity_camera*180.0/math.pi))

        leadout_angle_deg = self.leadout_subscriber.message.data*180.0/math.pi
        if leadout_angle_deg > 180.0:
            leadout_angle_deg -= 360

        self.leadout_publisher.publish(std_msgs.msg.Float32(data=leadout_angle_deg))
        self.pitch_publisher.publish(std_msgs.msg.Float32(data=pitch*180.0/math.pi))
        self.roll_publisher.publish(std_msgs.msg.Float32(data=roll*180.0/math.pi))
