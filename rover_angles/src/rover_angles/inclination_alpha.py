#! /usr/bin/env python3

import math

import rospy

import std_msgs.msg
import sensor_msgs.msg

from create_common_rospy.simple_subscriber import SimpleSubscriber
from tf.transformations import euler_from_quaternion

class InclinationAlpha:
    def __init__(self) -> None:
        self.imu_subscriber = SimpleSubscriber("/zed2i/zed_node/imu/data", sensor_msgs.msg.Imu)
    
        self.inclination_alpha_rad_publisher = rospy.Publisher("/rover/inclination_alpha/rad", std_msgs.msg.Float32, queue_size=10)
        self.inclination_alpha_deg_publisher = rospy.Publisher("/rover/inclination_alpha/deg", std_msgs.msg.Float32, queue_size=10)

        timer_period = rospy.Duration(0.04)
        self.timer = rospy.Timer(timer_period, self.timer_callback)

    def timer_callback(self, event: rospy.timer.TimerEvent) -> None:
        acc_x = self.imu_subscriber.message.linear_acceleration.x
        acc_y = self.imu_subscriber.message.linear_acceleration.y
        acc_z = self.imu_subscriber.message.linear_acceleration.z

        inclination = math.atan2(math.sqrt(acc_x*acc_x+acc_y*acc_y), acc_z)     

        self.inclination_alpha_rad_publisher.publish(std_msgs.msg.Float32(data=inclination))
        self.inclination_alpha_deg_publisher.publish(std_msgs.msg.Float32(data=inclination*180.0/math.pi))
