#!/bin/bash



while : ; do

	trap ' ' INT
	echo "Starting experiment with F_t=${1}"

	roslaunch rover_launch experiment.launch tether_force:=${1}
	echo "Finished recording bag."

	roslaunch rover_launch reset_experiment.launch
	echo "Reset experiment."

	trap - $(compgen -A signal)

	ls /home/create/bags/Ft_${1}* | wc -l
	read -n1 -s -r -p $'Press any key to start next run...\n' key

done
exit 0

