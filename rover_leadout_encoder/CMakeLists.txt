cmake_minimum_required(VERSION 3.0.2)
project(rover_leadout_encoder)

find_package(catkin REQUIRED COMPONENTS
  rosserial_arduino
  rosserial_client
)

catkin_package()

set(ARDUINO_LIBRARIES_PATH /home/create/Arduino/libraries)
set(ARDUINO_EXECUTABLE /opt/arduino-1.8.19/arduino)
set(BOARD /dev/rover/leadout_encoder)

add_custom_target(${PROJECT_NAME}_generate_ros_lib ALL
  COMMAND rm -rf ${ARDUINO_LIBRARIES_PATH}/ros_lib
  COMMAND /opt/ros/melodic/./share/rosserial_arduino/make_libraries.py
  ${ARDUINO_LIBRARIES_PATH}
)

add_custom_target(${PROJECT_NAME}_firmware ALL
  COMMAND ${ARDUINO_EXECUTABLE} --upload ${CMAKE_CURRENT_SOURCE_DIR}/firmware/RoverLeadoutEncoder/RoverLeadoutEncoder.ino --port ${BOARD}
  DEPENDS ${PROJECT_NAME}_generate_ros_lib
)
