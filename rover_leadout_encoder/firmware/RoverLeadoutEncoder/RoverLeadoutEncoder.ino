#include <ros.h>
#include <std_msgs/Float32.h>
#include <std_srvs/Empty.h>

#include <Arduino.h>

#include <ams_as5048b.h>


AMS_AS5048B myEncoder;

ros::NodeHandle  nh;
std_msgs::Float32 rawMsg;
ros::Publisher encoderRawPublisher("/rover/leadout_encoder/raw", &rawMsg);

void tare_callback( const std_srvs::EmptyRequest& req, std_srvs::EmptyResponse& res);
ros::ServiceServer<std_srvs::EmptyRequest, std_srvs::EmptyResponse> tareServiceServer("/rover/leadout_encoder/tare", &tare_callback);

void setup() {
  nh.initNode();
  nh.advertise(encoderRawPublisher);
  nh.advertiseService<std_srvs::EmptyRequest,std_srvs::EmptyResponse>(tareServiceServer);
  while (!nh.connected()) {
      nh.spinOnce();
      delay(10);
  }

  myEncoder.begin();
  myEncoder.setClockWise(true); 
  myEncoder.setZeroReg(); 
}

void tare_callback( const std_srvs::EmptyRequest& req, std_srvs::EmptyResponse& res){
  myEncoder.setZeroReg();
}

void loop() { 
  myEncoder.updateMovingAvgExp(); //otherwise Arduino returns zero
  float encoderAngle = myEncoder.angleR(U_RAD, false) ;
  rawMsg.data = encoderAngle;
  encoderRawPublisher.publish( &rawMsg );
  delay(18); // to publish at around 50Hz

  nh.spinOnce();
}